﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMProg
{
    class MainModule
    {
        static void Main(string[] args)
        {
            ATMApplication app = new ATMApplication();
            app.run();
        }
    }
}
