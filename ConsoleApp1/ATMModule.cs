﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMProg
{
    class Atm
    {
        object bank;
        const int SELECT_ACCOUNT_OPTION = 1;
        const int CREATE_ACCOUNT_OPTION = 2;
        const int EXIT_ATM_APPLICATION_OPTION = 3;

        const int CHECK_BALANCE_OPTION = 1;
        const int WITHDRAW_OPTION = 2;
        const int DEPOSIT_OPTION = 3;
        const int EXIT_ACCOUNT_OPTION = 4;

        public void start()
        {
            /*
        while True:

            selectedOption = self.showMainMenu()

            if selectedOption == self.SELECT_ACCOUNT_OPTION:
                acct = self.selectAccount()
                if acct != None:
                    self.manageAccount(acct)
            elif selectedOption == self.CREATE_ACCOUNT_OPTION:
                self.onCreateAccount()
            elif selectedOption == self.EXIT_ATM_APPLICATION_OPTION:
                return
            else:
                print('Please enter a valid menu option', "\n")
            */
        }

        public int showMainMenu()
        {
            /*
                    while True:
            try:
                return int(input('\nMain Menu\n\n1: Select Account\n2: Create Account\n3: Exit\n\nEnter a choice: ')) 
            except ValueError:
                #if the user enters "abc" instead of a number
                print("Please enter a valid menu option.", "\n");
            */
        }

        public int showAccountMenu()
        {
            /*
            while True:
            try:
                return int(input('\nAccount Menu\n\n1: Check Balance\n2: Withdraw\n3: Deposit\n4: Exit\n\nEnter a choice: ')) 
            except ValueError:
                print("Please enter a valid menu option.", "\n");
            */
        }

        public void onCreateAccount()
        {
            /*
                    while True:
            try:
                clientName = self.promptForClientName()

                initDepositAmount = self.promptForDepositAmount()

                annIntrRate = self.promptForAnnualIntrRate()

                newAccount = self._bank.openAccount(clientName, annIntrRate)        

                newAccount.deposit(initDepositAmount)
                
                return
                            
            except InvalidValue as err:
                print(err, "\n")

            except OperationCancel as err:
                print(err, "\n")
                return #the user has canceled the creation of the account           
            */
        }

        public object selectAccount()
        {
            /*
                while True:
                try:
                    acctNoInput = input('Please enter your account ID or press [ENTER] to cancel: ')

                    if len(acctNoInput) == 0:
                        return None

                    acctNo = int(acctNoInput)

                    acct = self._bank.findAccount(acctNo)
                    if acct != None:
                        return acct
                    else:
                        print('The account was not found. Please select another account.')
                except ValueError:
                    print('Please enter a valid account number (e.g. 100)', "\n")
            }
            */
        public void manageAccount(object account)
            {
                /*
            while True:
                selAcctMenuOpt = self.showAccountMenu()

                if selAcctMenuOpt == self.CHECK_BALANCE_OPTION:
                    self.onCheckBalance(account)
                elif selAcctMenuOpt == self.WITHDRAW_OPTION:
                    self.onWithdraw(account)
                elif selAcctMenuOpt == self.DEPOSIT_OPTION:
                    self.onDeposit(account)
                elif selAcctMenuOpt == self.EXIT_ACCOUNT_OPTION:
                    return
                else:
                    print('Please enter a valid menu option')
                */
            }

            public string promptForClientName()
            {
                /*
                clientName = input('Please enter the client name or press [ENTER] to cancel: ')
        
                if len(clientName) == 0:
                raise OperationCancel('The user has selected to cancel the current operation')
                */
                return clientName;
            }

            public float promptForDepositAmount()
            {
                /*
            while True:
                try:
                    initAmount = float(input('Please enter your initial account balance: '))

                    if initAmount >= 0:
                        return initAmount
                    else:
                        print('Cannot create an account with a negative initial balance. Please enter a valid amount')
                except ValueError as err:
                    print(err, "\n")
                */
            }

            public float promptForAnnualIntrRate()
            {
            /*
            while True:
            try:
                intrRate = float(input('Please enter the interest rate for this account: '))

                if intrRate >= 0:
                        return intrRate
                    else:
                        print('Cannot create an account with a negative interest rate.')
                except ValueError as err:
                    print(err, "\n")
             */
            }

            public void onCheckBalance(object account)
            {
            /*print('The balance is {0}\n'.format(account.getBalance()))*/
            }

            public void onDeposit(object account)
            {
            /*
                while True:
                try:
                    inputAmount = input('Please enter an amount to deposit or type [ENTER] to exit: ')

                if len(inputAmount) > 0:
                        amount = float(inputAmount)

                account.deposit(amount)

                return

                except ValueError:
                print('Invalid entry. Please enter a number for your amount.', "\n")

                except InvalidTransaction as err:
                    print(err, "\n")
             */
            }

            public void onWithdraw(object account)
            {
                /*
                while True:
                    try:
                        inputAmount = input('Please enter an amount to withdraw or type [ENTER] to exit: ')

                    if len(inputAmount) > 0:
                            amount = float(inputAmount)

                    account.withdraw(amount)

                    return

                    except ValueError:
                    print('Invalid entry. Please enter a number for your amount.', "\n")

                    except InvalidTransaction as err:
                        print(err, "\n")
                 */
            }


    }


}

