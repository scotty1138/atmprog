﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMProg
{
    class InvalidTransaction : Exception
    {
        //pass
    }

    class InvalidValue : Exception
    {
        //pass
    }

    class Account
    {
        const int ACCOUNT_TYPE_CHECQUING = 1;
        const int ACCOUNT_TYPE_SAVINGS = 2;

        int acctNo;
        string acctHolderName;
        float balance;
        float annualIntrRate;

        public Account(int acctNo = -1, string acctHolderName = "", float annualIntrRate = 0)
        {
            acctNo = acctNo;
            acctHolderName = acctHolderName;
            balance = 0;
            annualIntrRate = annualIntrRate;
        }

        public int getAccountNumber()
        {
            return acctNo;
        }

        public string getAcctHolderName()
        {
            return acctHolderName;
        }

        public float getBalance()
        {
            return balance;
        }

        public float getAnnualIntrRate()
        {
            return annualIntrRate;
        }

        public void setAnnualIntrRate(float newAnnualIntrRatePercentage)
        {
            //self._annualIntrRate = newAnnualIntrRatePercentage / 100
        }

        public float getMonthlyIntrRate()
        {
            //return self._annualIntrRate / 12
        }

        public float deposit(float amount)
        {
            /*
            if amount < 0:
                raise InvalidTransaction( 'Invalid amount provided. Cannot deposit a negative amount.')
        
            oldBalance = self._balance
            self._balance += amount
            */

            return balance;
        }

        public float withdraw(float amount)
        {
            /*
            if amount < 0:
            raise InvalidTransaction('Invalid amount provided. Cannot withdraw a negative amount.')

            if amount > self._balance:
                raise InvalidTransaction('Insufficient funds. Cannot withdraw the provided amount.')

            oldBalance = self._balance
            self._balance -= amount
            */

            return balance;
        }
    }
}
